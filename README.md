# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## Instructions:
---------------

- Author: Qi Han  Email: qhan@uoregon.edu
 
- Description: We need to use the bitbucket for all the projects we will have this term

  1. We need to create a new credentials.ini file to copy everything in the credentials-skel.ini file
  2. README file need to be modified
  3. We need to complie the hello.py file to make sure it prints out the message
  4. Once everything is done, commit and push the README file and hello.py file to the bitbucket

